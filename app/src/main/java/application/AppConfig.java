package application;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import repository.PersonRepository;
import repository.PersonRepositoryImpl;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Created by abednigo.masilo on 2017/06/20.
 */
@Configuration
@ComponentScan({"repository","services"}) // Scan the repository package (we have "customerRepository" bean in there... similar with services
public class AppConfig {

      private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("fitnessDashboard-persistence-unit");
      private static EntityManager em = emf.createEntityManager();

      @Bean(name = "personRepository")
    public PersonRepository setPersonRepEntityManager(){
          PersonRepository personRepository = new PersonRepositoryImpl();
          personRepository.setEntityManager(em);
          return personRepository;
      }

/*    @Bean(name = "userService")
    public UserService getCustomerService(){
        // Constructor Injection
        PersonServiceImpl service = new PersonServiceImpl();
        return service;
    }*/

   /* @Bean(name = "userRepository")
    public UserRepository getUserRepository(){
        return  new UserRepositoryImpl();
    }*/
}
