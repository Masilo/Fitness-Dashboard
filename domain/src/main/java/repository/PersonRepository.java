package repository;

import model.Person;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by abednigo.masilo on 2017/06/20.
 */

public interface PersonRepository {
    List<Person> findAll();
    Person findPerson(int userID);
    void setEntityManager(EntityManager entityManager);
}
