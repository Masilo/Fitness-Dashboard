package services;

import model.Person;

import java.util.List;

/**
 * Created by abednigo.masilo on 2017/06/20.
 */
public interface PersonService {
    List<Person> findAll();
    Person findPerson(int id);
}
