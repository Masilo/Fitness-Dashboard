package application;

import model.Device;
import model.Goal;
import model.Person;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import services.PersonService;

import java.util.Scanner;

/**
 * Created by abednigo.masilo on 2017/06/20.
 */
public class Application {
    public static void main(String args[]) {

        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(AppConfig.class);
        PersonService service = applicationContext.getBean("userService", PersonService.class);


        // for all users (Admin Dashboard)
//        for (Person person : service.findAll()) {
//            System.out.println("User: " + person.getFirstName() + " " + person.getLastName() + " has BMI "+ person.getFitnessService().getBmi(person.getHeight(),person.getWeight()));
//            System.out.println("\n---Devices---");
//            //Print all devices for user
//            for (Device device: person.getDevices()){
//                System.out.println("ID: " + device.getDeviceID() + " " + device.getMake() + " " + device.getName());
//            }
//            System.out.println("---End of Devices---\n");
//
//            //Print all goals for user
//            System.out.println("\n---Goals---");
//            for (Goal goal: person.getGoals()){
//                System.out.println("ID: " + goal.getGoalID() + " " + goal.getName() + " starting: " + goal.getStartDate());
//            }
//            System.out.println("---End of Goals---\n");
//        }

        // for a particular user (User # Dashboard)
        while (true) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("\nPlease enter user ID: ");
            int id = scanner.nextInt();

            Person person = service.findPerson(id);
            double userBmi = person.getFitnessService().getBmi(person.getHeight(), person.getWeight());
            System.out.println("User: " + person.getFirstName() + " " + person.getLastName());
            System.out.println("\nBMI: " + userBmi+"\n");
            System.out.println("Registered "+ person.getDevices().size() + " Device(s):");
            for (Device device: person.getDevices()){
                System.out.println("Device: " + " "+ device.getName());
            }
            System.out.println("\nHas "+ person.getGoals().size() + " Goal(s):");
            for (Goal goal: person.getGoals()){
                System.out.println("Goal: " + goal.getName() + " starting: " + goal.getStartDate()+"\n\n");
            }
        }
    }
}

/*
        // Console (UI) capturing of data
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter body weight: ");
        double weight = scanner.nextDouble();

        System.out.println("Please enter body height: ");
        double height = scanner.nextDouble();

*/

        // Run subscription
       // ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("/spring/si-components.xml");

       // while (true){

       // }
    //}