package services;

import model.Person;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;


/**
 * Created by abednigo.masilo on 2017/06/23.
 */
@Component
@ComponentScan({"repository","services"})
public class ServiceActivator {
    public boolean doSomething(Message<?> message){

        System.out.println("Device Data Received");
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(ServiceActivator.class);
        PersonService service = applicationContext.getBean("userService", PersonService.class);
        int id = 1;
        Person user = service.findAll().get(id);
        //System.out.println(user.getFirstname()+" has max HR: "+ user.getFitnessService().getMaxHeartRate(user.getAge()) +" based on Age ("+user.getAge()+")");

        String filename = ((File) message.getPayload()).getPath();
        File file = new File(filename);

        try{
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()){
                System.out.println("Average HR: " + scanner.nextLine());
            }
        }
        catch (FileNotFoundException e){
            System.out.println("Config file might not be found, please check file and try again");
        }
        return true;
    }
}
