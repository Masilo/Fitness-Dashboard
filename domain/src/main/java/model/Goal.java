package model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Bokang Masilo on 2017/07/05.
 */

@Entity
@Table(name = "Goal")
public class Goal {
    @Id
    private int goalID;
    private int userID;
    private String name;
    private String type;
    private Date startDate;
    private Date finishDate;

    public Goal(int goalID, int userID, String name, String type, Date startDate, Date finishDate) {
        this.goalID = goalID;
        this.userID = userID;
        this.name = name;
        this.type = type;
        this.startDate = startDate;
        this.finishDate = finishDate;
    }

    public Goal(){

    }

    public int getGoalID() {
        return goalID;
    }

    public void setGoalID(int goalID) {
        this.goalID = goalID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(Date finishDate) {
        this.finishDate = finishDate;
    }
}
