package repository;

import model.Device;
import model.Goal;
import model.Person;
import org.hibernate.criterion.CriteriaQuery;
import services.FitnessServiceImpl;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;
import org.springframework.stereotype.Repository;

/**
 * Created by abednigo.masilo on 2017/06/20.
 */

@Repository("personRepository")
public class PersonRepositoryImpl implements PersonRepository {

    private EntityManager em;

    public void setEntityManager(EntityManager entityManager){
        this.em = entityManager;
    }

    //data retrieval from DB
    public List<Person> findAll() {
        List<Person> persons;
        Query query = em.createQuery("select p from Person p");
        persons = query.getResultList();

        // Enrol to FitnessService With His Devices
        for (Person person: persons){
            person.setFitnessService(new FitnessServiceImpl());
            person.setDevices(findAllDevices(person.getUserID()));
            person.setGoals(findAllGoals(person.getUserID()));
        }
        return persons;
    }
    // find one user
    public Person findPerson(int userID) {
        Person person = em.find(Person.class,userID);
        person.setFitnessService(new FitnessServiceImpl());
        person.setDevices(findAllDevices(userID));
        person.setGoals(findAllGoals(userID));
        return person;
    }

    private List<Device> findAllDevices(int userID) {
        List<Device> devices;
        Query query = em.createQuery("SELECT d from Device d where d.UserID = :arg1").setParameter("arg1", userID);
        devices = query.getResultList();
        return devices;
    }

    private List<Goal> findAllGoals(int userID) {
        List<Goal> goals;
        Query query = em.createQuery("SELECT g from Goal g where g.userID = :arg1").setParameter("arg1", userID);
        goals = query.getResultList();
        return goals;
    }
}

