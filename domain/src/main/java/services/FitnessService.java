package services;

/**
 * Created by abednigo.masilo on 2017/06/21.
 */
public interface FitnessService {
    double getBmi(double height, double weight);
    double getMaxHeartRate(int age);
}
