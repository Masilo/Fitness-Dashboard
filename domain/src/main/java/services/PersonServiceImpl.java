package services;

import model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import repository.PersonRepository;

import java.util.List;

/**
 * Created by abednigo.masilo on 2017/06/20.
 */

@Component("userService")
public class PersonServiceImpl implements PersonService {
    //private UserRepository userRepository = new PersonRepositoryImpl(); Hard coded
    // The PersonServiceImpl shouldn't know that its using PersonRepositoryImpl Specifically

    @Autowired
    //config for auto Injection
    private PersonRepository personRepository;

    //@Autowired
    //config for constructor Injection
    public PersonServiceImpl(PersonRepository userRepository){
        System.out.println("Using Constructor Injection");
        this.personRepository = userRepository;
    }

    //config for setter Injection
    public PersonServiceImpl(){

    }

    //@Autowired
    //config for setter Injection
    public void setUserRepository(PersonRepository userRepository){
        System.out.println("Using Setter Injection");
        this.personRepository = userRepository;
    }

    public List<Person> findAll(){
        return personRepository.findAll();
    }
    public Person findPerson(int id){
        return personRepository.findPerson(id);
    }
}
