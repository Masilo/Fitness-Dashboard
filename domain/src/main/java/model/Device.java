package model;

import javax.persistence.*;

/**
 * Created by abednigo.masilo on 2017/07/03.
 */
@Entity
@Table( name = "Device")
public class Device {
    @Id
    //@GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "DeviceID")
    private int DeviceID;
    @Column(name = "UserID")
    private int UserID;
    @Column(name = "Name")
    private String Name;
    @Column(name = "Make")
    private String Make;

    public Device(int deviceID, int userID, String name, String make) {
        this.DeviceID = deviceID;
        this.UserID = userID;
        this.Name = name;
        this.Make = make;
    }
    public Device(){

    }

    public int getDeviceID() {
        return DeviceID;
    }

    public void setDeviceID(int deviceID) {
        DeviceID = deviceID;
    }

    @Column(name = "UserID")
    public int getUserID() {
        return UserID;
    }

    public void setUserID(int userID) {
        UserID = userID;
    }

    @Column(name = "Name")
    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    @Column(name = "Make")
    public String getMake() {
        return Make;
    }

    public void setMake(String make) {
        Make = make;
    }
}
