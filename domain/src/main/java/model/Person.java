package model;


import services.FitnessService;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Bokang Masilo on 2017/07/05.
 */
@Entity
@Table(name = "Person")
public class Person {
    @Id
    private int UserID;
    private String firstName;
    private String lastName;
    private String email;
    private double weight;
    private double height;
    private int age;

    @Transient
    private FitnessService fitnessService;
    @Transient
    //@OneToMany( targetEntity=Device.class ) //https://www.tutorialspoint.com/jpa/jpa_entity_relationships.htm
    private List<Device> devices;
    @Transient
    private List<Goal> goals;


    public Person(int userID, String firstName, String lastName, String email, double weight, double height, int age) {
        UserID = userID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.weight = weight;
        this.height = height;
        this.age = age;
    }

    public Person(){

    }

    public int getUserID() {
        return UserID;
    }

    public void setUserID(int userID) {
        UserID = userID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public FitnessService getFitnessService() {
        return fitnessService;
    }

    public void setFitnessService(FitnessService fitnessService) {
        this.fitnessService = fitnessService;
    }
    public List<Device> getDevices(){
        return devices;
    }
    public void setDevices(List<Device> devices){
        this.devices = devices;
    }

    public List<Goal> getGoals() {
        return goals;
    }

    public void setGoals(List<Goal> goals) {
        this.goals = goals;
    }
}
