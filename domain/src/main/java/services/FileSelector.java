package services;

import org.springframework.integration.core.MessageSelector;
import org.springframework.messaging.Message;

import java.io.File;

/**
 * Created by abednigo.masilo on 2017/06/23.
 */
public class FileSelector implements MessageSelector{
    public boolean accept(Message<?> message){
        // Filter text files based on their name
        if(message.getPayload() instanceof File && ((File) message.getPayload()).getName().contains("24-06-2017")){
            return true;
        }
        return false;
    }
}
