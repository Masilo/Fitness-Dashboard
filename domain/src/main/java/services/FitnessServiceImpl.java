package services;

import java.text.DecimalFormat;

/**
 * Created by abednigo.masilo on 2017/06/21.
 */
public class FitnessServiceImpl implements FitnessService {

    public FitnessServiceImpl(){

    }

    public double getMaxHeartRate(int age) {
        double heartRate = (207 - ((age)*0.7));
        return heartRate;
    }

    public double getBmi(double height, double weight){
        double bmiLong = (weight/(height*height));
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        double bmi = Double.valueOf(decimalFormat.format(bmiLong));
        return bmi;
    }
}
